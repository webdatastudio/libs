<?php

namespace App\Model\Repository;

/**
 * Základní třída repozitáře.
 *
 * @filesource	BaseRepository.php
 * @author		© Web Data Studio, www.web-data.cz
 * @version		1.1.0
 */
abstract class BaseRepository extends \YetORM\Repository
{

	/**
	 * Nastavení hodnot entity
	 * @param \YetORM\Entity $entity
	 * @param \Nette\Utils\ArrayHash|array $values
	 */
	public function setValues(\YetORM\Entity $entity, $values)
	{
		foreach ((is_array($values) ? $values : (array) $values) as $name => $value) {
			$property = $entity->getReflection()->getEntityProperty($name);

			if (!$property->isReadonly()) {
				$value = ($value == '' && $property->isNullable() ? null : $value);

				if (!is_null($value)) {
					$type = $property->getType();

					if (in_array(trim($type, '\\'), ['Nette\Utils\DateTime'])) {
						$value = \Nette\Utils\DateTime::from($value);
					} else {
						settype($value, $type);
					}
				}

				$entity->$name = $value;
			}
		}
	}

	/**
	 * @param \YetORM\Entity $entity
	 * return bool
	 */
	public function persist(\YetORM\Entity $entity)
	{
		foreach ($entity->toRecord()->getModified() as $name => $value) {

			if ((string) $value == '' && $entity->getReflection()->getEntityProperty($name)->isNullable()) {
				$entity->$name = null;
			}
		}

		parent::persist($entity);
	}

}
