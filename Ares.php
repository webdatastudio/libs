<?php

namespace App\Libs;

/**
 * Ověření a získání informací z ARES dle IČ.
 *
 * @filesource	Ares.php
 * @author		© Web Data Studio, www.web-data.cz
 * @version		1.1.0
 */
final class Ares extends \Nette\Object
{

	/** Adresa ARES pro získání XML */
	const ARES_URL = 'http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_bas.cgi?ico=';

	/**
	 * Načtení informací z ARES
	 * @param int $ic
	 * @return null|array
	 */
	public function getInfo($ic)
	{
		if ($this->isValid($ic)) {
			if ($xml = simplexml_load_file(self::ARES_URL . $ic)) {
				$xmlNamespace = $xml->getDocNamespaces();
				$data = $xml->children($xmlNamespace['are']);

				$el = $data->children($xmlNamespace['D'])->VBAS;

				return [
					'ic' => (string) $el->ICO,
					'dic' => (string) $el->DIC,
					'company' => (string) $el->OF,
					'street' => trim((string) $el->AA->NCO . ' ' . (string) $el->AA->CD),
					'city' => (string) $el->AA->N,
					'zip' => (string) $el->AA->PSC,
				];
			}
		}

		return null;
	}

	/**
	 * Kontrola správnosti IČ
	 * @param int|string $ic
	 * @return boolean|int
	 */
	public function isValid($ic)
	{
		$ic = preg_replace('#\s+#', '', $ic);

		if (preg_match('#^\d{8}$#', $ic)) {
			$sum = 0;
			for ($i = 0; $i < 7; $i++) {
				$sum += $ic[$i] * (8 - $i);
			}

			$mod = $sum % 11;

			if ($mod === 0) {
				$c = 1;
			} elseif ($mod === 10) {
				$c = 1;
			} elseif ($mod === 1) {
				$c = 0;
			} else {
				$c = 11 - $mod;
			}

			return (int) $ic[7] === $c;
		}

		return false;
	}

}
