<?php

/**
 * Vytvoření .zip archivu.
 *
 * @filesource	zip.php
 * @author		© Web Data Studio, www.web-data.cz
 * @version		1.1.0
 */
$path = realpath(__DIR__);

$maxExecutionTime = @ini_get('max_execution_time');
@ini_set('max_execution_time', 240);

$zip = new ZipArchive;
$zip->open('archive.zip', ZipArchive::CREATE);

foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::LEAVES_ONLY) as $file) {
	$localname = trim(str_replace($path, null, (string) $file), '\\, /');

	if ($localname != 'zip.php' && is_file($file)) {
		$zip->addFile($file, $localname);
		echo "Add file -> $localname", PHP_EOL;
	}
}

$zip->close();

@ini_set('max_execution_time', $maxExecutionTime);
