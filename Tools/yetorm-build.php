<?php

/**
 * Vytvoření repozitářů a entit z tabulek databáze.
 *
 * @filesource	yetorm-build.php
 * @author		© Web Data Studio, www.web-data.cz
 * @version		1.1.0
 */
/** Databázový ovladač */
define('DB_DRIVER', 'mysql');

/** Adresa SQL serveru */
define('DB_HOST', 'localhost');

/** Název databáze */
define('DB_NAME', 'dbname');

/** Přihlašovací jméno */
define('DB_USER', 'root');

/** Přihlašovací heslo */
define('DB_PASSWORD', 'root');

/** Namespace tříd */
define('NS', 'App\\');

// Přípojení do databáze,
$db = new PDO(DB_DRIVER . ':host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASSWORD, [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8']);

// Seznam tabulek pro které už byl model generovaný,
$ignoreTables = [];

if (file_exists($ignoreFile = __DIR__ . '\model\.ignore_tables')) {
	$ignoreTables = unserialize(file_get_contents($ignoreFile));
}

// Načtení názvu tabulek,
$tables = [];

foreach ($db->query('SHOW TABLES')->fetchAll(PDO::FETCH_COLUMN) as $table) {
	$name = implode('', array_map(function ($word) {
				return ucfirst($word);
			}, explode('_', $table)));

	if (!in_array($name, $ignoreTables)) {
		$cols = [];

		foreach ($db->query("DESCRIBE `$table`") as $col) {
			$cols[$col['Field']] = '@property' . (strtolower($col['Extra']) == 'auto_increment' ? '-read' : null)
					. ' ' . getColType(strtolower($col['Type'])) . (strtolower($col['Null']) == 'yes' ? '|NULL' : null)
					. ' $' . $col['Field'] . "\n";
		}

		$tables[$name] = [
			$table => $cols,
		];

		$ignoreTables[] = $name;
	}
}

// Generování tříd,
if (count($tables)) {

	foreach (['\model', '\model\entity'] as $path) {

		if (!file_exists(__DIR__ . $path)) {
			mkdir(__DIR__ . $path);
		}
	}

	foreach ($tables as $className => $table) {

		foreach ($table as $name => $cols) {
			// Repozitář,
			$properties = " * @table $name\n * @entity \\" . NS . 'Model\Entity\\' . $className;
			$buffer = 'namespace ' . NS . "Model\\Repository;\n\n/**\n$properties\n */\nfinal class $className extends \\"
					. NS . "Model\\Repository\\BaseRepository\n{\n\n}";
			file_put_contents(__DIR__ . '/model/' . $className . 'Repository.php', "<?php\n\n$buffer\n");

			// Entita,
			$properties = ' * ' . implode(' * ', $cols);
			$buffer = 'namespace ' . NS . "Model\\Entity;\n\n/**\n$properties */\nfinal class $className extends \\"
					. NS . "Model\\Entity\\BaseEntity\n{\n\n}";
			file_put_contents(__DIR__ . '/model/entity/' . $className . 'Entity.php', "<?php\n\n$buffer\n");
		}
	}

	file_put_contents($ignoreFile, serialize($ignoreTables));
}

/**
 * Detekce typu sloupce
 * @param string $type typ sloupce
 * @param array $outputArray
 * @return string
 */
function getColType($type, $outputArray = null)
{
	preg_match("/\w+/", $type, $outputArray);

	$types = [
		'string' => ['varchar', 'text'],
		'\Nette\Utils\DateTime' => ['datetime'],
	];

	foreach ($types as $_type => $array) {

		if (in_array(strtolower(trim($outputArray[0])), $array)) {
			$outputArray[0] = $_type;
			break;
		}
	}

	return $outputArray[0];
}
