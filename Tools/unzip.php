<?php

/**
 * Rozbalení .zip archivu.
 *
 * @filesource	unzip.php
 * @author		© Web Data Studio, www.web-data.cz
 * @version		1.1.0
 */
if (($zip = zip_open('archive.zip')) !== FALSE) {

	$maxExecutionTime = @ini_get('max_execution_time');
	@ini_set('max_execution_time', 240);

	$files = $directory = 0;

	while ($zipEntry = zip_read($zip)) {
		zip_entry_open($zip, $zipEntry);

		$entryName = zip_entry_name($zipEntry);

		if (substr($entryName, -1) == '/') {
			$zipDir = substr($entryName, 0, -1);

			if (!file_exists($zipDir)) {
				echo "Create directory $zipDir<br />";
				mkdir($zipDir);
				$directory++;
			}
		} else {
			$entrySize = zip_entry_filesize($zipEntry);

			echo "Copy file $entryName size $entrySize<br />";
			file_put_contents($entryName, zip_entry_read($zipEntry, $entrySize));
			$files++;
		}

		zip_entry_close($zipEntry);
	}

	zip_close($zip);

	@ini_set('max_execution_time', $maxExecutionTime);

	echo "<br />Directory(es): $directory, File(s): $files";
} else {
	echo '<br />File archive.zip not found.<br />';
}
